# Lending Library

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Install and run
1. `npm install`
2. `npm start`

## Backend
[https://gitlab.com/Dorine31/backend-django](https://gitlab.com/Dorine31/backend-django)

## Dependencies
#### react-router :
[website](https://reacttraining.com/react-router/web/guides/quick-start)
[https://www.npmjs.com/package/react-router](https://www.npmjs.com/package/react-router)
[https://www.npmjs.com/package/react-router-dom](https://www.npmjs.com/package/react-router-dom)

#### semantic-ui :
[website](https://react.semantic-ui.com/)

#### superagent :
[https://www.npmjs.com/package/superagent](https://www.npmjs.com/package/superagent)

## Other commands :
- npm test
- npm run build
- npm run eject
