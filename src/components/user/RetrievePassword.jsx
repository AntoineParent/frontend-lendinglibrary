import React, { useState } from 'react'

const RetrievePassword = () => {
  const [email, setEmail] = useState('')
  const [password1, setPassword1] = useState('')
  const [password2, setPassword2] = useState('')

  const handleSubmit = event => {
    event.preventDefault()
    console.log(email, password1)
  }
  const handleChange = event => {
    switch (event.currentTarget.name) {
      case 'email':
        setEmail(event.currentTarget.value)
        break;
      case 'password1':
        setPassword1(event.currentTarget.value)
        break;
      case 'password2':
        setPassword2(event.currentTarget.value)
        break;
      default:
        break;
    }
  };

  return (
    <div>
      <h2>Retrieve your password</h2>
      <form onSubmit={handleSubmit} action="">
        <label>Your email :</label><br />
        <input type="email"
          name="email"
          placeholder="Your email"
          value={email}
          onChange={handleChange} /><br />
        <label>Your password :</label><br />
        <input type="password"
          name="password1"
          placeholder="Enter your password"
          value={password1}
          onChange={handleChange} /><br />
        <input type="password"
          name="password2"
          placeholder="Re-enter your password"
          value={password2}
          onChange={handleChange} /><br />
        <button>Change my password</button>
      </form>
    </div>
  )
}

export default RetrievePassword
