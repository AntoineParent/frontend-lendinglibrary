/* eslint-disable no-useless-escape */
import React, { useState } from "react";
import {
  Link,
  useHistory
} from "react-router-dom";
import { Container, Button, Form } from 'semantic-ui-react'

const Login = ({ fetchUserData }) => {
  let history = useHistory()

  const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  const PASSWORD_REGEX = /^(?=.*\d).{4,8}$/

  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [emailError, setEmailError] = useState('')
  const [passwordError, setPasswordError] = useState('')

  const checkField = () => {
    if (!EMAIL_REGEX.test(email)) {
      setEmailError('Invalid email address')
    } else {
      setEmailError('')
    }
    if (!PASSWORD_REGEX.test(password)) {
      setPasswordError('Invalid password')
    } else {
      setPasswordError('')
    }
  }

  //SUBMIT
  const handleSubmit = (e) => {
    e.preventDefault()
    checkField()
    if (!emailError && !passwordError) {
      //
      fetch("http://localhost:3060/api/login", {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        credentials: 'include',
        body: JSON.stringify({ email, password })
      })
        .then(res => {
          //console.log(res.json());
          return res.json()
        })
        .then(data => {
          if (data.error !== undefined) {
            switch (data.error) {
              case 10:
                setEmailError('Email unknown')
                break;
              case 20:
                setEmailError('Inactive account')
                break;
              case 30:
                setPasswordError('Wrong password')
                break;
              case 50:
                setEmailError('Internal server error')
                break;
              default:
                break
            }
          } else {
            fetchUserData()
            setTimeout(history.push('/dashboard'), 10000);
          }
        })
    }
  }

  const handleChange = event => {
    switch (event.currentTarget.name) {
      case 'email':
        setEmail(event.currentTarget.value)
        break;
      case 'password':
        setPassword(event.currentTarget.value)
        break;
      default:
        break;
    }
  };

  return (
    <Container text>
      <h1>Login</h1>
      <div className="classic-container">
        <Form action="" onSubmit={handleSubmit}>
          <Form.Field>
            <label htmlFor="">Email :</label>
            <input
              type="email"
              name="email"
              placeholder="Your email"
              value={email}
              onChange={handleChange}
            />
            <div style={{ color: 'red' }}>{emailError}</div>
          </Form.Field>
          <Form.Field>
            <label htmlFor="">Password :</label>
            <input
              type="password"
              name="password"
              placeholder="Your password"
              value={password}
              onChange={handleChange}
            />
            <div style={{ color: 'red' }}>{passwordError}</div>
          </Form.Field>
          <Button color="teal" size="large" type='submit'>Sign in</Button>
        </Form>

        <br />
        Not a member? <Link to="/signup">Sign up now</Link>
      </div>
    </Container>
  );
}

export default Login