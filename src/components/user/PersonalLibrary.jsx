import React, { useState, useEffect } from 'react'
import { Container } from 'semantic-ui-react'
import ResultCardEdit from './ResultCardEdit'


export default function PersonalLibrary({ setEditedCopy }) {
  const [booklist, setBooklist] = useState([])

  useEffect(() => {
    fetch('http://localhost:3060/api/bookshelf', {
      method: 'GET',
      credentials: 'include'
    })
      .then(res => res.json())
      .then(data => {
        setBooklist(data)
      })
  }, [setBooklist])

  return (
    <Container>
      <h3>Personal Library / Bookshelf ?</h3>
      <div className="resultscontainer">
        {
          booklist.map((item, i) => {
            return (
              <ResultCardEdit
                key={i}
                id={item.copy.id}
                image={item.book.thumbnail}
                title={item.book.title}
                authors={item.book.authors}
                copydata={item.copy}
                bookdatadata={item.book}
                click={() => setEditedCopy(item)}
              />
            )
          })
        }
      </div>
    </Container>
  )
}
