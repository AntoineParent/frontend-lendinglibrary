import React from 'react'
import UserSettings from './UserSettings'
import PersonalLibrary from './PersonalLibrary'
import { Container, Divider } from 'semantic-ui-react'

export default function Dashboard({ userData, fetchUserData, setEditedCopy }) {

  return (
    <>
      <h1>Dashboard</h1>
      <div className="classic-container">
        <Container>
          <br />
          <UserSettings userData={userData} fetchUserData={fetchUserData} />
          <br />
          <Divider />
          <br />
          <PersonalLibrary setEditedCopy={setEditedCopy} />
        </Container>
      </div>
      <div className="headeruser"></div>
    </>
  );
}