import React, { useState, useEffect } from 'react'
import { Button, Container, Form, Icon, List } from 'semantic-ui-react'

//class UserSettings extends Component {
export default function UserSettings({ userData, fetchUserData }) {
  // GET USER DATA IN VARIABLES
  const [data, setData] = useState({ userData })
  // BOOLEAN TO EDIT OR NOT
  const [editInfo, setEditInfo] = useState(false)
  const [editPassword, setEditPassword] = useState(false)
  const [editAddress, setEditAddress] = useState(false)

  //info
  const [tempUsername, setTempUsername] = useState('')
  const [tempFirstname, setTempFirstname] = useState('')
  const [tempLastname, setTempLastname] = useState('')
  const [tempEmail, setTempEmail] = useState('')
  const [tempPhone, setTempPhone] = useState('')

  //password
  const [tempNewPassword, setTempNewPassword] = useState('')
  const [tempNewPassword2, setTempNewPassword2] = useState('')

  //Address
  const [tempZipcode, setTempZipcode] = useState('')
  const [tempTown, setTempTown] = useState('')


  const duplicateData = () => {
    //Info
    setTempUsername(data.username)
    setTempFirstname(data.first_name)
    setTempLastname(data.last_name)
    setTempEmail(data.email)
    setTempPhone(data.phone)
    //Address
    setTempZipcode(data.zipcode)
    setTempTown(data.town)
  }

  useEffect(() => {
    setData(userData)
  }, [userData])

  function handleChange(event) {
    switch (event.currentTarget.name) {
      case "username":
        setTempUsername(event.currentTarget.value)
        break
      case "first_name":
        setTempFirstname(event.currentTarget.value)
        break
      case "last_name":
        setTempLastname(event.currentTarget.value)
        break
      case "email":
        setTempEmail(event.currentTarget.value)
        break
      case "phone":
        setTempPhone(event.currentTarget.value)
        break
      case "tempNewPassword":
        setTempNewPassword(event.currentTarget.value)
        break
      case "tempNewPassword2":
        setTempNewPassword2(event.currentTarget.value)
        break
      case "zipcode":
        setTempZipcode(event.currentTarget.value)
        break
      case "town":
        setTempTown(event.currentTarget.value)
        break
      default:
        break
    }
  }

  function handleSubmitAddress(event) {
    event.preventDefault();
    console.log(data.id, tempZipcode, tempTown)
    fetch('http://localhost:3060/api/update-user-address', {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      credentials: 'include',
      body: JSON.stringify({ id: data.id, zipcode: tempZipcode, town: tempTown })
    })
      .then(res => {
        switch (res.status) {
          case 200:
            fetchUserData()
            setEditAddress(false)
            console.log('ok')
            break
          case 400:
            // il faudra prévenir
            // visuellement l'utilisateur
            console.log('error')
            break
          default:
            break;
        }
      })
  }
  function handleSubmitPassword(event) {
    event.preventDefault();
    // console.log(tempNewPassword, tempNewPassword2) check if they are similar
    fetch('http://localhost:3060/api/update-user-password', {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      credentials: 'include',
      body: JSON.stringify({ id: data.id, password: tempNewPassword })
    })
      .then(res => {
        switch (res.status) {
          case 200:
            fetchUserData()
            setEditPassword(false)
            console.log('ok')
            break
          case 400:
            // il faudra prévenir
            // visuellement l'utilisateur
            console.log('error')
            break
          default:
            break;
        }
      })
  }
  function handleSubmitInfo(event) {
    event.preventDefault();
    console.log(data.id, tempUsername, tempFirstname, tempLastname, tempEmail, tempPhone)

    fetch('http://localhost:3060/api/update-user-info', {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      credentials: 'include',
      body: JSON.stringify({ id: data.id, username: tempUsername, first_name: tempFirstname, last_name: tempLastname, email: tempEmail, phone: tempPhone })
    })
      .then(res => {
        switch (res.status) {
          case 200:
            fetchUserData()
            setEditInfo(false)
            console.log('ok')
            break
          case 400:
            // il faudra prévenir
            // visuellement l'utilisateur
            console.log('error')
            break
          default:
            break;
        }
      })
  }

  //--------------------------USERINFO
  function displayInfo() {
    return (
      <>
        <List divided relaxed>
          <List.Item>
            <List.Icon name='user' size='large' verticalAlign='middle' color="grey" />
            <List.Content>
              <List.Header>Username :</List.Header>
              <List.Description>{data.username === undefined ? '-' : data.username}</List.Description>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Icon name='arrow alternate circle right' size='large' verticalAlign='middle' color="grey" />
            <List.Content>
              <List.Header>Firstname :</List.Header>
              <List.Description>{data.first_name === undefined ? '-' : data.first_name}</List.Description>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Icon name='arrow alternate circle right' size='large' verticalAlign='middle' color="grey" />
            <List.Content>
              <List.Header>Lastname :</List.Header>
              <List.Description>{data.last_name === undefined ? '-' : data.last_name}</List.Description>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Icon name='mail' size='large' verticalAlign='middle' color="grey" />
            <List.Content>
              <List.Header>Email :</List.Header>
              <List.Description>{data.email}</List.Description>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Icon name='mobile' size='large' verticalAlign='middle' color="grey" />
            <List.Content>
              <List.Header>Phone number :</List.Header>
              <List.Description>{data.phone === undefined ? '-' : data.phone}</List.Description>
            </List.Content>
          </List.Item>
        </List>
        <Button color="teal" size="mini" onClick={() => { duplicateData(); setEditInfo(true); }}><Icon name='edit' />Edit</Button>
      </>
    )
  }
  function displayEditInfo() {
    return (
      <>
        <Form action="" onSubmit={handleSubmitInfo}>
          <Form.Input type="text" fluid label='Username :' name="username" value={tempUsername} onChange={handleChange} />
          <Form.Input type="text" fluid label='Firstname :' name="first_name" value={tempFirstname} onChange={handleChange} />
          <Form.Input type="text" fluid label='Lastname :' name="last_name" value={tempLastname} onChange={handleChange} />
          <Form.Input type="email" fluid label='Email :' name="email" value={tempEmail} onChange={handleChange} />
          <Form.Input type="tel" fluid label='Phone number :' name="phone" value={tempPhone} onChange={handleChange} />
          <Button size="mini" onClick={() => setEditInfo(false)}><Icon name="close" />Cancel</Button>
          <Button color="teal" size="mini" type="submit">Save change</Button>
        </Form>
      </>
    )
  }

  //--------------------------PASSWORD
  function displayPassword() {
    return (
      <>
        <List divided relaxed>
          <List.Item>
            <List.Icon name='lock' size='large' verticalAlign='middle' color="grey" />
            <List.Content>
              <List.Description>************</List.Description>
            </List.Content>
          </List.Item>
        </List>
        <Button color="teal" size="mini" onClick={() => { setEditPassword(true); duplicateData() }}><Icon name='edit' />Edit</Button>
      </>
    )
  }
  function displayEditPassword() {
    return (
      <>
        <Form action="" onSubmit={handleSubmitPassword}>
          <Form.Input type="password" fluid label='New password :' name="tempNewPassword" value={tempNewPassword} onChange={handleChange} />
          <Form.Input type="password" fluid label='Comfirm new password :' name="tempNewPassword2" value={tempNewPassword2} onChange={handleChange} />
          <Button size="mini" onClick={() => setEditPassword(false)}><Icon name="close" />Cancel</Button>
          <Button color="teal" size="mini" type="submit">Save change</Button>
        </Form>
      </>
    )
  }

  //--------------------------ADDRESS
  function displayAddress() {
    return (
      <>
        <List divided relaxed>
          <List.Item>
            <List.Icon name='map marker alternate' size='large' verticalAlign='middle' color="grey" />
            <List.Content>
              <List.Header>Zipcode :</List.Header>
              <List.Description>{data.zipcode === undefined ? '-' : data.zipcode}</List.Description>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Icon name='map' size='large' verticalAlign='middle' color="grey" />
            <List.Content>
              <List.Header>Town :</List.Header>
              <List.Description>{data.town === undefined ? '-' : data.town}</List.Description>
            </List.Content>
          </List.Item>
        </List>
        <Button color="teal" size="mini" onClick={() => { setEditAddress(true); duplicateData() }}><Icon name='edit' />Edit</Button>
      </>
    )
  }
  function displaySetAddress() {
    return (
      <>
        <Form action="" onSubmit={handleSubmitAddress}>
          <Form.Input type="text" fluid label='Zipcode :' name="zipcode" value={tempZipcode} onChange={handleChange} />
          <Form.Input type="text" fluid label='Town :' name="town" value={tempTown} onChange={handleChange} />
          <Button size="mini" onClick={() => setEditAddress(false)}><Icon name="close" />Cancel</Button>
          <Button color="teal" size="mini" type="submit">Save change</Button>
        </Form>
      </>
    )
  }

  return (
    <Container>
      <h3>Personal information :</h3>
      {
        editInfo ? displayEditInfo() : displayInfo()
      }
      <h3>Password :</h3>
      {
        editPassword ? displayEditPassword() : displayPassword()
      }
      <h3>Address :</h3>
      {
        editAddress ? displaySetAddress() : displayAddress()
      }
    </Container >
  )
}