/* eslint-disable no-useless-escape */
import React, { useState } from "react";
import {
  Link,
  useHistory
} from "react-router-dom";
import { Container, Button, Form, Input } from 'semantic-ui-react'

const Signup = () => {
  let history = useHistory();

  const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  const PASSWORD_REGEX = /^(?=.*\d).{4,8}$/

  const [username, setUsername] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [firstname, setFirstname] = useState('')
  const [lastname, setLastname] = useState('')
  const [town, setTown] = useState('')
  const [zipcode, setZipcode] = useState('')
  const [phone, setPhone] = useState('')
  const [usernameError, setUsernameError] = useState('')
  const [emailError, setEmailError] = useState('')
  const [passwordError, setPasswordError] = useState('')
  const [firstnameError] = useState('')
  const [lastnameError] = useState('')
  const [townError] = useState('')
  const [zipcodeError, setZipcodeError] = useState('')
  const [phoneError] = useState('')

  const checkField = () => {
    if (!username) {
      setUsernameError('required')
    } else {
      setUsernameError('')
    }
    if (!EMAIL_REGEX.test(email)) {
      setEmailError('Invalid email address')
    } else {
      setEmailError('')
    }
    if (!PASSWORD_REGEX.test(password)) {
      setPasswordError('Invalid password')
    } else {
      setPasswordError('')
    }
    if (!zipcode) {
      setZipcodeError('required')
    } else {
      setZipcodeError('')
    }
  }

  const handleSubmit = (e) => {
    e.preventDefault()
    checkField()
    const new_user = {
      username: username,
      email: email,
      password: password,
      first_name: firstname,
      last_name: lastname,
      town: town,
      zipcode: zipcode,
      phone: phone
    }
    if (!usernameError && !emailError && !passwordError && !zipcodeError) {
      // Il faudra tester les données avant le fetch
      fetch("http://localhost:3060/api/create-account", {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        credentials: 'include',
        body: JSON.stringify(new_user)
      })
        .then(res => {
          switch (res.status) {
            case 200:
              history.push("/login")
              break
            case 400:
              // il faudra prévenir
              // visuellement l'utilisateur
              console.log('error')
              break
            default:
              break;
          }
        })
    }
  }

  const handleChange = event => {
    switch (event.currentTarget.name) {
      case 'username':
        setUsername(event.currentTarget.value)
        break;
      case 'firstname':
        setFirstname(event.currentTarget.value)
        break;
      case 'lastname':
        setLastname(event.currentTarget.value)
        break;
      case 'email':
        setEmail(event.currentTarget.value)
        break;
      case 'password':
        setPassword(event.currentTarget.value)
        break;
      case 'town':
        setTown(event.currentTarget.value)
        break;
      case 'zipcode':
        setZipcode(event.currentTarget.value)
        break;
      case 'phone':
        setPhone(event.currentTarget.value)
        break;

      default:
        break;
    }
  };

  return (
    <Container text>
      <h1>Sign up</h1>
      <div className="classic-container">
        <Form action="" onSubmit={handleSubmit}>
          <Form.Field>
            <label htmlFor="">Username :</label>
            <Input
              type="text"
              name="username"
              placeholder="Username"
              value={username}
              onChange={handleChange}
            />
            <div style={{ color: 'red' }}>{usernameError}</div>
          </Form.Field>
          <Form.Field>
            <label htmlFor="">Email :</label>
            <Input
              type="text"
              name="email"
              placeholder="Email"
              value={email}
              onChange={handleChange}
            />
            <div style={{ color: 'red' }}>{emailError}</div>
          </Form.Field>
          <Form.Field>
            <label htmlFor="">Password :</label>
            <Input
              type="password"
              name="password"
              placeholder="login"
              value={password}
              onChange={handleChange}
            />
            <div style={{ color: 'red' }}>{passwordError}</div>
          </Form.Field>
          <Form.Field>
            <label htmlFor="">Firstname :</label>
            <Input
              type="text"
              name="firstname"
              placeholder="Firstname"
              value={firstname}
              onChange={handleChange}
            />
            <div style={{ color: 'red' }}>{firstnameError}</div>
          </Form.Field>
          <Form.Field>
            <label htmlFor="">Lastname :</label>
            <Input
              type="text"
              name="lastname"
              placeholder="Lastname"
              value={lastname}
              onChange={handleChange}
            />
            <div style={{ color: 'red' }}>{lastnameError}</div>
          </Form.Field>
          <Form.Field>
            <label htmlFor="">Town :</label>
            <Input
              type="text"
              name="town"
              placeholder="town"
              value={town}
              onChange={handleChange}
            />
            <div style={{ color: 'red' }}>{townError}</div>
          </Form.Field>
          <Form.Field>
            <label htmlFor="">Zip code :</label>
            <Input
              type="text"
              name="zipcode"
              placeholder="zipcode"
              value={zipcode}
              onChange={handleChange}
            />
            <div style={{ color: 'red' }}>{zipcodeError}</div>
          </Form.Field>
          <Form.Field>
            <label htmlFor="">Phone :</label>
            <Input
              type="text"
              name="phone"
              placeholder="phone"
              value={phone}
              onChange={handleChange}
            />
            <div style={{ color: 'red' }}>{phoneError}</div>
          </Form.Field>
          <Button color="teal" size="large" type='submit'>Sign up</Button>
        </Form>
        <br />
        Already a member? <Link to="/login">Sign in</Link>
      </div>
    </Container>
  );
}

export default Signup