import React from "react";
import { Link } from "react-router-dom"

const ResultCardEdit = (props) => {
  return (
    <button onClick={props.click} className="cardbook">
      <Link to="/edit-copy">
        <div className="imgcontainer">
          <img src={props.image} alt="" />
        </div>
        <div className="authors">{props.authors}</div>
        <div className="title">{props.title}</div>
      </Link>
    </button>
  )
}

export default ResultCardEdit;
