import React from 'react'
import { Container, Header } from 'semantic-ui-react'
import { Link } from "react-router-dom";

const Page404 = () => {

  return (
    <Container text>
      <Header as="h2">Page 404</Header>
      <h3>It seems you are lost</h3>
      <Link to="/">Go to The Corner Book</Link>
    </Container>
  )
}

export default Page404