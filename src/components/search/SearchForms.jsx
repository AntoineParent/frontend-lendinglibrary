
import React, { useState } from 'react'
import { Form, Input, Button } from 'semantic-ui-react'
import request from 'superagent'

const Search = ({ setBooks }) => {
  const [searchTerms, setSearchTerms] = useState('')
  const [option, setOption] = useState('title')

  const handleChange = (event) => setSearchTerms(event.currentTarget.value)
  const handleRadio = (e, { option }) => setOption(option)

  const handleSubmit = (event) => {
    event.preventDefault()
    request
      .get("http://localhost:3060/api/search-book")
      .query({ terms: searchTerms, option: option })
      .then(data => {
        setBooks(data.body)
      });
  }

  return (
    <div className="searchform">
      <h1>The best way<br />to sold and buy books</h1>
      <Form onSubmit={handleSubmit} action="">
        <Input
          type="text"
          name="searchTerms"
          value={searchTerms}
          placeholder="Search : title, authors, category, isbn..."
          onChange={handleChange}
          className="searchfield"
        />
        <Button color="teal" className="searchbutton">Search</Button>
        <Form.Group className="searchoption">
          <label>Search for :</label>
          <Form.Radio
            option='title'
            color="teal"
            checked={option === 'title'}
            onChange={handleRadio}
            className="searchradio"
          />
          <label htmlFor="title">Title</label>

          <Form.Radio
            option='authors'
            checked={option === 'authors'}
            onChange={handleRadio}
            className="searchradio"
          />
          <label htmlFor="authors">Author</label>

          <Form.Radio
            option='isbn_13'
            checked={option === 'isbn_13'}
            onChange={handleRadio}
            className="searchradio"
          />
          <label htmlFor="isbn_13">ISBN</label>

          <Form.Radio
            option='description'
            checked={option === 'description'}
            onChange={handleRadio}
            className="searchradio"
          />
          <label htmlFor="description">Text</label>

        </Form.Group>
      </Form>
    </div>
  );
}

export default Search