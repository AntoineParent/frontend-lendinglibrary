import React from "react";
import ResultCard from './ResultCard'

const ResultList = ({ books, setSelectedBook }) => {
  return (
    <div className="resultscontainer">
      {books.map((elt, i) => {
        return (
          <ResultCard
            key={i}
            id={elt.copy.book_id}
            image={elt.book.thumbnail}
            title={elt.book.title}
            authors={elt.book.authors}
            setSelectedBook={() => setSelectedBook(elt)}
          />
        )
      })}
    </div>
  )
}

export default ResultList