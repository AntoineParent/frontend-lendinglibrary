import React from 'react'
import SearchForm from './SearchForms'
import ResultsList from './ResultList'

const Search = (props) => {
  return (
    <>
      <SearchForm setBooks={props.setBooks} />
      <ResultsList books={props.books} setSelectedBook={props.setSelectedBook} />
      <div className="headersearch"></div>
    </>
  );
}

export default Search