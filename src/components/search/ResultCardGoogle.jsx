import React from "react";
import { Link } from "react-router-dom";

const ResultCard = (props) => {
  return (
    <button onClick={props.setEditedAdd} className="cardbook">
      <Link to="/edit-add">
        <div className="imgcontainer">
          <img src={props.image} alt="" />
        </div>
        <div className="authors">{props.authors}</div>
        <div className="title">{props.title}</div>
      </Link>
    </button>
  )
}

export default ResultCard;
