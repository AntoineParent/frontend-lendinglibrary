import React from 'react'
import MapDetail from './MapDetail'
import InfoDetail from './InfoDetail'
import UserDetail from './UserDetail'
import { Container } from 'semantic-ui-react'
import { Link } from "react-router-dom";

const BookDetail = ({ selectedBook }) => {
  const book = selectedBook.book
  const user = selectedBook.user
  const copy = selectedBook.copy
  return (
    <>
      <Container text>
        <Link to="/" className="backbutton">Back</Link>
        <InfoDetail book={book} copy={copy} />
        <UserDetail user={user} />
        <br />
        <MapDetail user={user} />
      </Container>
      <div className="headerdetail"></div>
    </>
  )
}

export default BookDetail
