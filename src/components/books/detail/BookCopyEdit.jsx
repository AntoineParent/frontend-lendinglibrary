import React, { useState } from 'react'
import { Link } from "react-router-dom";
import { Container, Form, Button, Message, Divider, List } from 'semantic-ui-react'

const BookCopyEdit = ({ item }) => {
  const [hideMessage, setHideMessage] = useState(true)
  const [deleted, setDeleted] = useState(false)
  const [modified, setModified] = useState(false)
  const [condition, setCondition] = useState(item.copy.condition)
  const [availability, setAvailability] = useState(item.copy.availability)

  const conditionOption = [
    { key: 'good', value: 'good', text: 'Good' },
    { key: 'used', value: 'used', text: 'Used' },
    { key: 'damaged', value: 'damaged', text: 'Damaged' },
  ]

  const selectChangeHandler = (e, result) => {
    const value = result.value
    setCondition(value)
    setModified(true)
    setHideMessage(true)
  }
  const checkboxChangeHandler = (e, data) => {
    const value = data.checked
    setAvailability(value)
    setModified(true)
    setHideMessage(true)
  }

  const updateCopy = (event) => {
    event.preventDefault()
    console.log("j'update", condition, availability)
    fetch("http://localhost:3060/api/update-copy", {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      credentials: 'include',
      body: JSON.stringify({ id: item.copy.id, condition, availability })
    })
      .then(res => {
        switch (res.status) {
          case 200:
            //history.push("/login")
            console.log('ok update !')
            setModified(false)
            setHideMessage(false)
            console.log(hideMessage)

            break
          case 400:
            // il faudra prévenir
            // visuellement l'utilisateur
            console.log('error')
            break
          default:
            break;
        }
      })
  }
  const deleteCopy = () => {
    fetch("http://localhost:3060/api/delete-copy", {
      method: 'DELETE',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      credentials: 'include',
      body: JSON.stringify({ id: item.copy.id })
    })
      .then(res => {
        switch (res.status) {
          case 200:
            //history.push("/login")
            setDeleted(true)
            break
          case 400:
            // il faudra prévenir
            // visuellement l'utilisateur
            console.log('error')
            break
          default:
            break;
        }
      })
  }

  return (
    <Container text>
      <Link to="/dashboard" className="backbutton">Back</Link>
      {
        item ?
          <div>
            <h2>Your copy of {item.book.title}</h2>
            <div className="authorswhite">{item.book.authors}</div>
            <div className="infodetail">
              <img src={item.book.thumbnail} alt="" className="imgcontainer" />
              <div className="infocontent">
                <p>{item.book.description}</p>
              </div>
            </div>
            <br />
            <Divider />
            <List divided relaxed>
              <List.Item>
                <List.Header>Isbn :</List.Header>
                <List.Description>{item.book.isbn_13}</List.Description>
              </List.Item>
              <List.Item>
                <List.Header>Language :</List.Header>
                <List.Description>{item.book.language}</List.Description>
              </List.Item>
              <List.Item>
                <List.Header>Page count :</List.Header>
                <List.Description>{item.book.pageCount}</List.Description>
              </List.Item>
              <List.Item>
                <List.Header>Published date :</List.Header>
                <List.Description>{item.book.publishedDate}</List.Description>
              </List.Item>
              <List.Item>
                <List.Header>Publisher :</List.Header>
                <List.Description>{item.book.publisher}</List.Description>
              </List.Item>
            </List >
            <br />

            <h3>About your copy</h3>

            <Form action="" onSubmit={updateCopy}>
              <Form.Field>
                <label>Added at :</label>
                {item.copy.created_at}
              </Form.Field>
              <label>Condition :</label>
              <Form.Field>
                <Form.Select name="condition" onChange={selectChangeHandler} placeholder={item.copy.condition} options={conditionOption} />
              </Form.Field>
              <Form.Field>
                <Form.Checkbox name="availability" onChange={checkboxChangeHandler} defaultChecked={item.copy.availability === 1 ? true : false} label='This copy is available' />
              </Form.Field>
              <Button disabled={!modified} type="submit" color="teal">Save change</Button>
            </Form>
            {
              hideMessage ?
                null
                :
                <Message color="teal">
                  <Message.Header>Changes have been saved !</Message.Header>
                </Message>
            }
            <Button onClick={() => deleteCopy()} color="red">Delete your copy</Button>
            {
              deleted ?
                <Message color="red">
                  <Message.Header>Your copy have been deleted !</Message.Header>
                </Message>
                :
                null
            }
            <br />
            <br />
          </div>

          : null
      }
    </Container>
  )
}

export default BookCopyEdit
