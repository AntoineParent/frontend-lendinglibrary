import React from 'react'
import { Divider, List } from 'semantic-ui-react'

const InfoDetail = ({ book, copy }) => {
  return (
    <div>
      {
        book ?
          <div>
            <h2>{book.title}</h2>
            <div className="authorswhite">{book.authors}</div>

            <div className="infodetail">
              <img src={book.thumbnail} alt="" className="imgcontainer" />
              <div className="infocontent">
                <p>{book.description}</p>
              </div>
            </div>
            <br />
            <Divider />
            <List divided relaxed>
              <List.Item>
                <List.Header>Isbn :</List.Header>
                <List.Description>{book.isbn_13}</List.Description>
              </List.Item>
              <List.Item>
                <List.Header>Language :</List.Header>
                <List.Description>{book.language}</List.Description>
              </List.Item>
              <List.Item>
                <List.Header>Page count :</List.Header>
                <List.Description>{book.pageCount}</List.Description>
              </List.Item>
              <List.Item>
                <List.Header>Published date :</List.Header>
                <List.Description>{book.publishedDate}</List.Description>
              </List.Item>
              <List.Item>
                <List.Header>Publisher :</List.Header>
                <List.Description>{book.publisher}</List.Description>
              </List.Item>
            </List >
            <br />
            <h3>About this copy</h3>
            <List divided relaxed>
              <List.Item>
                <List.Header>Condition :</List.Header>
                <List.Description>{copy.condition}</List.Description>
              </List.Item>
              <List.Item>
                <List.Header>Availability :</List.Header>
                <List.Description>{copy.availability === 1 ? 'available' : 'unavailable'}</List.Description>
              </List.Item>
            </List >
          </div >
          : null
      }
    </div>
  )
}

export default InfoDetail
