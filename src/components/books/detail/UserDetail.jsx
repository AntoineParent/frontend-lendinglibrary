import React from 'react'
import { List } from 'semantic-ui-react'

const UserDetail = ({ user }) => {
  const url_phone = `tel:${user.phone}`
  const url_mail = `mailto:${user.email}`
  return (
    <div>
      {user ?
        <div>
          <br />
          <h3>Owner :</h3>
          <List divided relaxed>
            <List.Item>
              <List.Header>Username :</List.Header>
              <List.Description>{user.username}</List.Description>
            </List.Item>
            <List.Item>
              <List.Header>Address :</List.Header>
              <List.Description>{user.town} ({user.zipcode})</List.Description>
            </List.Item>
          </List>
          {user.phone ? <a href={url_phone} className="contactbutton">{user.phone}</a> : null}
          {user.email ? <a href={url_mail} className="contactbutton">{user.email}</a> : null}
        </div>
        : null
      }
      <br />
    </div >
  )
}

export default UserDetail
