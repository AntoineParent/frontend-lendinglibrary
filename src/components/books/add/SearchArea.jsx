import React from "react";
import { Container, Form, Input, Button } from "semantic-ui-react";

const SearchArea = props => {
  return (
    <Container textAlign="center">
      <h1>Add a book to your personal library</h1>
      <Form onSubmit={props.searchBook}>
        <Input
          placeholder="Title, author, category, IBSN..."
          onChange={props.handleSearch}
          className="searchfield"
        />
        <Button type="submit" color="teal" className="searchbutton">Search in Google Book API</Button>
      </Form>
    </Container>
  );
};

export default SearchArea;
