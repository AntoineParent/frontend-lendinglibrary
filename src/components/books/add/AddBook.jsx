import React, { useState } from "react";

/* modules */
import request from "superagent";

/* components */
import SearchArea from "./SearchArea";
import BookList from "./BookList";

export default function AddBook({ setEditedAdd }) {
  const [books, setBooks] = useState([])
  const [searchField, setSearchField] = useState("")

  const searchBook = e => {
    e.preventDefault();
    request
      .get("https://www.googleapis.com/books/v1/volumes")
      .query({ q: searchField, startIndex: 0, maxResults: 40 })
      .then(data => {
        const dataCleaned = processData(data);
        setBooks(dataCleaned);
      })
      .catch(err => {
        console.log(err)
      })
    // AIzaSyDqYBRXmOvR16-plBezviMaX_hSZPXUVzo
    // AIzaSyCExDjoUsyxcCG8CIabw5vQO45gIEVav9s
    /* fetch(`https://www.googleapis.com/books/v1/volumes?q=${this.state.searchField}`, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })*/
  };

  const processData = data => {
    const googleBook = data.body.items
    const appBook = []

    googleBook.map((book, i) => {
      let bookObj = {}
      const no_data = null

      bookObj["front_id"] = i
      //api
      bookObj["textSnippet"] = no_data
      bookObj["authors"] = no_data
      bookObj["description"] = no_data
      bookObj["thumbnail"] = 'https://via.placeholder.com/150x210'
      bookObj["isbn_10"] = no_data
      bookObj["isbn_13"] = no_data
      bookObj["pageCount"] = no_data
      bookObj["publishedDate"] = no_data
      bookObj["publisher"] = no_data
      bookObj["title"] = no_data
      bookObj["language"] = no_data

      //searchInfo
      if (book.searchInfo !== undefined && book.searchInfo.textSnippet !== undefined)
        bookObj["textSnippet"] = book.searchInfo.textSnippet
      //volumeInfo
      if (book.volumeInfo !== undefined) {
        if (book.volumeInfo.authors !== undefined)
          bookObj["authors"] = book.volumeInfo.authors.join(", ")
        if (book.volumeInfo.description !== undefined)
          bookObj["description"] = book.volumeInfo.description
        // -- image
        if (book.volumeInfo.imageLinks !== undefined) {
          if (book.volumeInfo.imageLinks.thumbnail !== undefined)
            bookObj["thumbnail"] = book.volumeInfo.imageLinks.thumbnail
        }
        // -- isbn
        if (book.volumeInfo.industryIdentifiers !== undefined) {
          book.volumeInfo.industryIdentifiers.map(id => {
            if (id.type === 'ISBN_10')
              bookObj["isbn_10"] = id.identifier
            if (id.type === 'ISBN_13')
              bookObj["isbn_13"] = id.identifier

            return id
          })
        }
        if (book.volumeInfo.language !== undefined)
          bookObj["language"] = book.volumeInfo.language
        if (book.volumeInfo.pageCount !== undefined)
          bookObj["pageCount"] = book.volumeInfo.pageCount
        if (book.volumeInfo.publishedDate !== undefined)
          bookObj["publishedDate"] = book.volumeInfo.publishedDate
        if (book.volumeInfo.publisher !== undefined)
          bookObj["publisher"] = book.volumeInfo.publisher
        if (book.volumeInfo.title !== undefined)
          bookObj["title"] = book.volumeInfo.title
      }
      appBook.push(bookObj)
      return bookObj
    })
    return appBook
  }

  const handleSearch = e => {
    setSearchField(e.target.value);
  };

  return (
    <div>
      <SearchArea
        className="searchbar"
        searchBook={searchBook}
        handleSearch={handleSearch}
      />
      <BookList books={books} setEditedAdd={setEditedAdd} />
      <div className="headerpersonal"></div>
    </div >
  );
}