import React from "react";
import ResultCardGoogle from '../../search/ResultCardGoogle'

const BookList = ({ books, setEditedAdd }) => {
  return (
    <div className="resultscontainer google">
      {books.map((book, i) => {
        return (
          <ResultCardGoogle
            key={i}
            id={book.front_id}
            image={book.thumbnail}
            title={book.title}
            authors={book.authors}
            setEditedAdd={() => setEditedAdd(book)}
          />
        )
      })
      }
    </div>
  );
};

export default BookList;
