import React, { useState } from 'react'
import { Link } from "react-router-dom";
import { Container, Form, Button, Message, Divider, List } from 'semantic-ui-react'

const BookAddEdit = ({ item }) => {
  const [hideMessage, setHideMessage] = useState(true)
  const [modified, setModified] = useState(false)
  const [condition, setCondition] = useState('')
  const [availability, setAvailability] = useState(true)

  const conditionOption = [
    { key: 'good', value: 'good', text: 'Good' },
    { key: 'used', value: 'used', text: 'Used' },
    { key: 'damaged', value: 'damaged', text: 'Damaged' },
  ]

  const selectChangeHandler = (e, result) => {
    const value = result.value
    setCondition(value)
    setModified(true)
    setHideMessage(true)
  }
  const checkboxChangeHandler = (e, data) => {
    const value = data.checked
    setAvailability(value)
    setModified(true)
    setHideMessage(true)
  }
  const getThisBook = (event) => {
    event.preventDefault()
    const new_book = item
    new_book['condition'] = condition
    new_book['availability'] = availability
    fetch("http://localhost:3060/api/addbook", {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      credentials: 'include',
      body: JSON.stringify(new_book)
    })
      .then(res => {
        switch (res.status) {
          case 200:
            setHideMessage(false)
            break
          default:
            break
        }
      })
      .catch(err => {
        console.log(err)
      })
  };
  return (
    <Container text>
      <Link to="/add-book" className="backbutton">Back</Link>
      {
        item ?
          <div>
            <h2>Add your copy of  {item.title}</h2>
            <div className="authorswhite">{item.authors}</div>
            <div className="infodetail">
              <img src={item.thumbnail} alt="" className="imgcontainer" />
              <div className="infocontent">
                <p>{item.description}</p>
              </div>
            </div>
            <br />
            <Divider />
            <List divided relaxed>
              <List.Item>
                <List.Header>Isbn :</List.Header>
                <List.Description>{item.isbn_13}</List.Description>
              </List.Item>
              <List.Item>
                <List.Header>Language :</List.Header>
                <List.Description>{item.language}</List.Description>
              </List.Item>
              <List.Item>
                <List.Header>Page count :</List.Header>
                <List.Description>{item.pageCount}</List.Description>
              </List.Item>
              <List.Item>
                <List.Header>Published date :</List.Header>
                <List.Description>{item.publishedDate}</List.Description>
              </List.Item>
              <List.Item>
                <List.Header>Publisher :</List.Header>
                <List.Description>{item.publisher}</List.Description>
              </List.Item>
            </List >
            <br />

            <h3>About your copy</h3>

            <Form action="" onSubmit={getThisBook}>
              <Form.Field>
              </Form.Field>
              <label>Condition :</label>
              <Form.Field>
                <Form.Select name="condition" onChange={selectChangeHandler} placeholder="Select a condition" options={conditionOption} />
              </Form.Field>
              <Form.Field>
                <Form.Checkbox name="availability" onChange={checkboxChangeHandler} defaultChecked={availability} label='This copy is available' />
              </Form.Field>
              <Button disabled={!modified} type="submit" color="teal">Add this book</Button>
            </Form>
            {
              hideMessage ?
                null
                :
                <Message color="teal">
                  <Message.Header>Your copy have been saved !</Message.Header>
                </Message>
            }
            <br />
            <br />
          </div>
          : null
      }
    </Container>
  )
}

export default BookAddEdit
