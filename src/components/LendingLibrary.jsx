import React, { useState, useEffect } from "react";
import { Icon } from 'semantic-ui-react'
import Cookies from 'js-cookie'

// Components
import Search from './search/Search'
import Login from './user/Login'
import Signup from './user/Signup'
import RetrievePassword from './user/RetrievePassword'
import Dashboard from './user/Dashboard'
import AddBook from './books/add/AddBook'
import BookDetail from './books/detail/BookDetail'
import BookCopyEdit from './books/detail/BookCopyEdit'
import BookAddEdit from './books/add/BookAddEdit'
import Page404 from './Page404'

// Router
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

export default function Book() {
  // USER DATA
  const [cookie] = useState(Cookies.get('app_session'))
  const [userState, setUserState] = useState(false)
  const [userData, setUserData] = useState({})

  // BOOK DATA
  const [books, setBooks] = useState([])
  const [selectedBook, setSelectedBook] = useState({})
  const [editedCopy, setEditedCopy] = useState({})
  const [editedAdd, setEditedAdd] = useState({})


  const fetchUserData = () => {
    fetch('http://localhost:3060/api/userdata', {
      method: 'GET',
      credentials: 'include'
    })
      .then(res => res.json())
      .then(data => {
        setUserState(true)
        setUserData(data)
      })
  }

  const signOut = () => {
    Cookies.remove('app_session')
    setUserData([])
    setUserState(false)
  }

  useEffect(() => {
    if (cookie) {
      fetch('http://localhost:3060/api/userdata', {
        method: 'GET',
        credentials: 'include'
      })
        .then(res => res.json())
        .then(data => {
          setUserData(data)
          setUserState(true)
        })
    }
  }, [cookie])

  return (
    <Router>
      <div className="page-container">

        <div className="navigation">
          <Link to="/"> <div className="logo"></div></Link>
          {
            !userState ?
              <div className="connect">
                Not a member ? <Link to="/signup" className="signup">Sign up now !</Link>
                <Link to="/login" className="sign">Sign in</Link>
              </div>
              :
              <div className="connect">
                <Link to="/" onClick={signOut} className="signout">Sign out</Link>
                <Link to="/dashboard" className="sign">My account</Link>
              </div>
          }
        </div>

        <div className="content">
          <Switch>
            <Route exact path="/">
              <Search books={books} setBooks={setBooks} setSelectedBook={setSelectedBook} />
            </Route>
            <Route path="/login">
              <Login fetchUserData={fetchUserData} />
              <div className="headeruser"></div>
            </Route>
            <Route path="/signup">
              <Signup />
              <div className="headeruser"></div>
            </Route>
            <Route path="/dashboard">
              <Dashboard userData={userData} fetchUserData={fetchUserData} setEditedCopy={setEditedCopy} />
            </Route>
            <Route path="/add-book">
              <AddBook setEditedAdd={setEditedAdd} />
            </Route>
            <Route path="/bookdetail">
              <BookDetail selectedBook={selectedBook} />
            </Route>
            <Route path="/retrieve-password">
              <RetrievePassword />
            </Route>
            <Route path="/edit-copy">
              <BookCopyEdit item={editedCopy} />
              <div className="headerpersonal"></div>
            </Route>
            <Route path="/edit-add">
              <BookAddEdit item={editedAdd} />
              <div className="headerpersonal"></div>
            </Route>
            <Route path="*" component={Page404} />
          </Switch>
        </div>
        {
          !userState ? null : <Link to="/add-book"><div className="addbutton"><Icon name="plus" size='big' /></div></Link>
        }
        <div className="footer">
          <a href="#!" className="link-footer">Legals</a>
          <a href="#!" className="link-footer">Credits</a>
          <a href="#!" className="link-footer">Contact us</a>
        </div>
      </div>
    </Router >
  );
}